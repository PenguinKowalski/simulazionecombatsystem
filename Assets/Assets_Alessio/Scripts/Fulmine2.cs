using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fulmine2 : Abilita
{
    [SerializeField] GameObject scudoFulmine;

    GameObject scudoFulmineSpawnato;

    [SerializeField] float durataAbilitą;

    public override void Attacca(Entita player, bool _isGrounded)
    {
        GameObject newScudoFulmine = Instantiate(scudoFulmine, player.gameObject.transform.position, Quaternion.identity);
        newScudoFulmine.transform.parent = player.gameObject.transform;
        scudoFulmineSpawnato = newScudoFulmine;
        StartCoroutine(ScudoFulmine());

    }
    
    IEnumerator ScudoFulmine()
    {
        Arena._instance.listaEntita[0].ChangeResistenza(0.5f, 0);
        yield return new WaitForSeconds(durataAbilitą);
        Arena._instance.listaEntita[0].ChangeResistenza(1f, 0);
        Destroy(scudoFulmineSpawnato);
        yield return null;
    }
}
