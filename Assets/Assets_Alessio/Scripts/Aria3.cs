using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aria3 : Abilita
{
    [SerializeField] GameObject maxiTempestaAria;

    [SerializeField] float danniTipoAria;

    [SerializeField] float forzaLancio;



    public override void Attacca(Entita player, bool _isGrounded)
    {
        Instantiate(maxiTempestaAria, new Vector2(0, 0), Quaternion.identity);

        if(Arena._instance.listaEntita.Count > 1)
        {
            for(int i = 1; i < Arena._instance.listaEntita.Count; i++)
            {
                Arena._instance.listaEntita[i].ChangeElementValue(2, danniTipoAria);
                Arena._instance.listaEntita[i].gameObject.GetComponent<Rigidbody2D>().AddForce((Arena._instance.listaEntita[i].gameObject.transform.position - player.transform.position).normalized * forzaLancio);
            }
        }
    }
}
