using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuoco2 : Abilita
{
    [SerializeField] GameObject conoDiFiamme;

    [SerializeField] GameObject conoDiFiammeSpawnato;

    public override void Attacca(Entita player, bool _isGrounded)
    {
        
        GameObject newConoDiFiamme = Instantiate(conoDiFiamme, player.gameObject.transform.position, Quaternion.identity);
        newConoDiFiamme.transform.parent = player.gameObject.transform;
        conoDiFiammeSpawnato = newConoDiFiamme;
    }

    public override void StopAbilita()
    {
        Destroy(conoDiFiammeSpawnato);
    }
}
