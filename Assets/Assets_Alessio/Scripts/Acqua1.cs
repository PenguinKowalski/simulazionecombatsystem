using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acqua1 : Abilita
{
    [SerializeField] GameObject bollaAcqua;

    GameObject bollaAcquaSpawnata;

    public override void Attacca(Entita player, bool _isGrounded)
    {
        Entita entitaVicina = null;

        float distanzaMinore = 1000;

        for (int i = 1; i < Arena._instance.listaEntita.Count; i++)
        {
            if (Vector2.Distance(player.gameObject.transform.position, Arena._instance.listaEntita[i].gameObject.transform.position) < distanzaMinore)
            {
                entitaVicina = Arena._instance.listaEntita[i];
                distanzaMinore = Vector2.Distance(player.gameObject.transform.position, Arena._instance.listaEntita[i].gameObject.transform.position);
            }
        }

        GameObject newBollaAcqua = Instantiate(bollaAcqua, entitaVicina.gameObject.transform.position, Quaternion.identity);
        newBollaAcqua.transform.parent = entitaVicina.gameObject.transform;
        newBollaAcqua.GetComponent<BollaAcqua>().EntitaDaDanneggiare = entitaVicina;
        bollaAcquaSpawnata = newBollaAcqua;

    }


    public override void DanneggiaNemico()
    {
        //Debug.Log("Lancio");
        //bollaAcquaSpawnata.GetComponent<BollaAcqua>().LanciaBolla();
    }
}
