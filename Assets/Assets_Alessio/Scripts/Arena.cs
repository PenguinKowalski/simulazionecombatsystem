using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour
{
    public static Arena _instance;

    [SerializeField] GameObject[] prefabNemici;

    //Elemento 0 � il giocatore
    public List<Entita> listaEntita;
    public List<Proiettile> listaProiettili = new List<Proiettile>();

    [SerializeField] Round[] round;

    [SerializeField] float delayRound;

    [SerializeField] SpawnPoint[] posizioniSpawn;

    [SerializeField] LayerMask Nemici;

    int currentRound = 0;

    [SerializeField] float X = 0, Y = 0;


    private void Awake()
    {
        _instance = this;
    }

    private void Update()
    {
        /*
        if(listaEntita.Count < 2 && currentRound < round.Length)
        {
            NewRound();
        }
        */
    }


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnNemici(0));
    }

    IEnumerator SpawnNemici(int roundAttuale)
    {

        
        for(int n = 0; n < prefabNemici.Length; n++)
        {
            for (int i = 0; i < (round[roundAttuale].NumeroSpawnNemici[n]) + Random.Range(0, round[roundAttuale].RangeRandom); i++)
            {
                StartCoroutine(SpwanNemicoAtLocationCoroutine(n));
                yield return new WaitForSeconds(1f);
            }
        }
        
        yield return null;

    }

    IEnumerator SpwanNemicoAtLocationCoroutine(int tipoNemico)
    {
        bool spawned = false;
        int i = Random.Range(0, posizioniSpawn.Length);

        while (!spawned)
        {
            if (CheckIfSpawnPointClear(posizioniSpawn[i].SpawnPosition.position) && !CheckIfSpawnPointNearPlayer(posizioniSpawn[i].SpawnPosition.position) && posizioniSpawn[i].CanSpawnThisEnemy(tipoNemico) && i < posizioniSpawn.Length)
            {
                GameObject newEntity = Instantiate(prefabNemici[tipoNemico], posizioniSpawn[i].SpawnPosition);
                Entita entity = newEntity.GetComponent<Entita>();
                AddEntity(entity);
                spawned = true;
            }
            if(i < posizioniSpawn.Length -1)
            {
                i++;
            }
            else
            {
                i = 0;
            }


            yield return new WaitForSeconds(1f);
        }

        yield return true;
    }

    bool CheckIfSpawnPointClear(Vector2 position)
    {
        if (Physics2D.BoxCast(position, new Vector2(1,1), 0, new Vector2(0,0), Nemici) == false)
        {
            //Debug.Log("pulito");
            return true;
        }
        else
        {
            //Debug.Log("sporco");
            return false;
        }       
    }


    bool CheckIfSpawnPointNearPlayer(Vector2 position)
    {
        if (Vector2.Distance(position, listaEntita[0].gameObject.transform.position) < 2.5f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void EndGame()
    {

    }

    void StartGame()
    {

    }

    public Entita GetPlayer()
    {
        if(listaEntita.Count > 0)
        {
            return listaEntita[0];
        }
        else
        {
            return null;
        }
    }

    void NewRound()
    {
        currentRound ++;
        StartCoroutine(SpawnNemici(currentRound));
    }

    public void AddEntity(Entita newEntity)
    {
        listaEntita.Add(newEntity);
    }

    public void RemoveEntity(Entita entityToRemove)
    {
        listaEntita.Remove(entityToRemove);
    }

    public void AddProjectiles(Proiettile newProiettile)
    {
        listaProiettili.Add(newProiettile);
    }

    public void RemoveProjectiles(Proiettile proiettileToRemove)
    {
        listaProiettili.Remove(proiettileToRemove);
    }

    public void StopMoveAll()
    {
        for(int i = 0; i < listaEntita.Count; i++)
        {
            listaEntita[i].StopMove();
        }
        for(int i = 0; i < listaProiettili.Count; i++)
        {
            listaProiettili[i].StopMove();
        }
    }

    public void StartMoveAll()
    {
        for (int i = 0; i < listaEntita.Count; i++)
        {
            listaEntita[i].StartMove();
        }
        for (int i = 0; i < listaProiettili.Count; i++)
        {
            listaProiettili[i].StartMove();
        }
    }
}
