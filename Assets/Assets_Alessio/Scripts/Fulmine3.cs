using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fulmine3 : Abilita
{
    [SerializeField] GameObject tempestaDiFulmini;

    [SerializeField] int minFulmini;

    [SerializeField] float timer;

    [SerializeField] float danniTipoFulmine;


    public override void Attacca(Entita player, bool _isGrounded)
    {
        Arena._instance.StopMoveAll();

        int n = Random.Range(minFulmini, Arena._instance.listaEntita.Count);

        StartCoroutine(DanneggiaNemici(n));
    }

    IEnumerator DanneggiaNemici(int n)
    {
        int i = 1;
        while(i <= n)
        {
            if (Arena._instance.listaEntita.Count > 1)
            {
                GameObject newTempestaDiFulmini = Instantiate(tempestaDiFulmini, Arena._instance.listaEntita[i].gameObject.transform.position, Quaternion.identity);
                Arena._instance.listaEntita[i].ChangeElementValue(1, danniTipoFulmine);

                i++;

                yield return new WaitForSeconds(timer);
            }
            if(Arena._instance.listaEntita.Count <= 0 || i == n)
            {
                Arena._instance.StartMoveAll();
            }
        }

        yield return null;

    }
}
