using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proiettile : MonoBehaviour
{
    [SerializeField] float danno;

    public Vector2 rbVelocity;

    Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Arena._instance.AddProjectiles(this);       
    }

    private void Update()
    {
        Destroy(this.gameObject, 30);
    }

    public void StopMove()
    {
        rbVelocity = rb.velocity;
        rb.isKinematic = true;
        rb.velocity = new Vector2(0, 0);
    }

    public void StartMove()
    {
        rb.isKinematic = false;
        rb.velocity = rbVelocity;
    }

    public void Death()
    {
        Arena._instance.RemoveProjectiles(this);
        Destroy(this.gameObject);
    }

    public void GoBack()
    {
        rb.isKinematic = false;
        rb.velocity = -rbVelocity;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Entita>() != null)
        {
            collision.gameObject.GetComponent<Entita>().ChangeHealth(danno);
            Death();
        }
    }
}
