using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuoco3 : Abilita
{
    [SerializeField] GameObject colonnaDiFuoco;

    [SerializeField] float durataAbilitą;

    public override void Attacca(Entita player, bool _isGrounded)
    {
        for(int i = 1; i < Arena._instance.listaEntita.Count; i++)
        {
            GameObject newColonnaDifuoco = Instantiate(colonnaDiFuoco, Arena._instance.listaEntita[i].gameObject.transform.position, Quaternion.identity);

            ColonnaDiFuoco colonna = newColonnaDifuoco.gameObject.GetComponent<ColonnaDiFuoco>();

            colonna.EntitaDaDanneggiare = Arena._instance.listaEntita[i];

            colonna.Paly();

            
        }

        StartCoroutine(ImmobilizzaNemici(durataAbilitą));
    }

    IEnumerator ImmobilizzaNemici(float durataAbilitą)
    {
        Arena._instance.StopMoveAll();
        yield return new WaitForSeconds(durataAbilitą);
        Arena._instance.StartMoveAll();
        yield return null;
    }
}
