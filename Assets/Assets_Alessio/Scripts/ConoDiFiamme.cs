using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConoDiFiamme : MonoBehaviour
{
    [SerializeField] float danniTipoFuoco;
    [SerializeField] float durataAbilitą;
    [SerializeField] float timer;
    [SerializeField] LayerMask Nemici;

    List<Entita> entitaDaDanneggiare;


    private void Start()
    {
        StartCoroutine(DanneggiaNemico(timer));
    }

    private void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector3 aimDirection = (mousePos - transform.position).normalized;
        float rotx = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, rotx);

        if(durataAbilitą > 0)
        {
            durataAbilitą -= Time.deltaTime;
        }
        if(durataAbilitą <= 0)
        {
            Destroy(this.gameObject);
        }
    }


    IEnumerator DanneggiaNemico(float timer)
    {
        while (this)
        {
            RaycastHit2D[] nemici = Physics2D.RaycastAll(transform.position, transform.right, 4f, Nemici);

            Debug.DrawRay(transform.position, transform.right * 4f, Color.magenta, 10f);

            for (int i = 0; i < nemici.Length; i++)
            {
                nemici[i].collider.gameObject.GetComponent<Entita>().ChangeElementValue(0, danniTipoFuoco);
            }

            yield return new WaitForSeconds(timer);
        }

        yield return null;
    }

}
