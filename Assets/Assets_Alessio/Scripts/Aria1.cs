using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aria1 : Abilita
{
    [SerializeField] GameObject scudoAria;

    public override void Attacca(Entita player, bool _isGrounded)
    {
        GameObject newScudoAria = Instantiate(scudoAria, Arena._instance.GetPlayer().gameObject.transform.position, Quaternion.identity);
        newScudoAria.transform.parent = Arena._instance.GetPlayer().gameObject.transform;
    }
}
