using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColonnaDiFuoco : MonoBehaviour
{
    Entita entitaDaDanneggiare;

    [SerializeField] float danniTipoFuoco;

    [SerializeField] float durataAbilitą;

    public Entita EntitaDaDanneggiare { get => entitaDaDanneggiare; set => entitaDaDanneggiare = value; }

    public void Paly()
    {
        //Debug.Log(entitaDaDanneggiare.name);
        DanneggiaNemiciColonnaDiFuoco();
        Destroy(this.gameObject, durataAbilitą);
    }

    void DanneggiaNemiciColonnaDiFuoco()
    {
        entitaDaDanneggiare.ChangeElementValue(0, danniTipoFuoco);
    }
}
