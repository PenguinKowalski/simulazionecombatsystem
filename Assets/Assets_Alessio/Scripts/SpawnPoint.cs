using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField] bool[] flagNemico;

    Transform spawnPosition;

    public Transform SpawnPosition { get => spawnPosition; set => spawnPosition = value; }

    // Start is called before the first frame update
    void Awake()
    {
        spawnPosition = transform;
    }

    public bool CanSpawnThisEnemy(int indexEnemy)
    {
        if (flagNemico[indexEnemy] == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(transform.position, 0.1f);
    }
}
