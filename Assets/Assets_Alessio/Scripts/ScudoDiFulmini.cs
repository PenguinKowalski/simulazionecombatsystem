using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScudoDiFulmini : MonoBehaviour
{
    [SerializeField] float danniTipoFulmine;

    [SerializeField] float forzaSpinta;

    [SerializeField] LayerMask Nemici;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("collisione con" + collision.gameObject.name);

        if (collision.gameObject.GetComponent<Entita>() != null && collision.gameObject.GetComponent<Entita>() != Arena._instance.listaEntita[0])
        {
            RespingiNemici(collision, forzaSpinta);
            DanneggiaNemici(danniTipoFulmine, collision);
        }
    }


    void RespingiNemici(Collider2D collision, float forzaSpinta)
    {
        collision.gameObject.GetComponent<Rigidbody2D>().AddForce((collision.gameObject.transform.position - transform.position).normalized * forzaSpinta);
    }

    void DanneggiaNemici(float danniTipoFulmine, Collider2D collision)
    {
        collision.gameObject.GetComponent<Entita>().ChangeElementValue(1, danniTipoFulmine);
    }
}
