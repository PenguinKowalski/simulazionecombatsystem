using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScudoDiAria : MonoBehaviour
{
    [SerializeField] float durataAbilita;

    // Update is called once per frame
    void Update()
    {
        Destroy(this.gameObject, durataAbilita);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Proiettile>() != null)
        {
            Arena._instance.RemoveProjectiles(collision.gameObject.GetComponent<Proiettile>());
            Destroy(collision.gameObject);
        }
    }
}
