using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acqua2 : Abilita
{
    [SerializeField] GameObject effettoGhiaccio;

    [SerializeField] float raggioAzione;

    [SerializeField] float danniNormali;

    [SerializeField] float danniAcqua;

    [SerializeField] LayerMask nemici;


    public override void Attacca(Entita player, bool _isGrounded)
    {
        Collider2D[] nemiciVicini = Physics2D.OverlapCircleAll(player.transform.position, raggioAzione, nemici);

        //Debug.Log("attacco");

        for(int i = 0; i < nemiciVicini.Length; i++)
        {
            Entita entitaCorrente = nemiciVicini[i].gameObject.GetComponent<Entita>();

            //Debug.Log(nemiciVicini[i].gameObject.name);

            entitaCorrente.ChangeElementValue(3, danniAcqua);
            entitaCorrente.ChangeHealth(danniNormali);
            entitaCorrente.CheckIfABIsFull(3);
            Instantiate(effettoGhiaccio, entitaCorrente.transform.position, Quaternion.identity);
        }
    }

}
