using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acqua3 : Abilita
{
    [SerializeField] GameObject maxiTsunami;

    [SerializeField] Vector2 posizioneSpawn;

    public override void Attacca(Entita player, bool _isGrounded)
    {
        GameObject newMaxiTsunami = Instantiate(maxiTsunami, posizioneSpawn, Quaternion.identity);
    }
}
