using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxiTsunami : MonoBehaviour
{
    [SerializeField] float danniTipoAcqua;

    [SerializeField] float velocitāSpostamento;

    [SerializeField] Vector3 direzioneSpostamento;

    [SerializeField] float DurataAbilita;

    private void Start()
    {
        Arena._instance.StopMoveAll();
    }

    private void Update()
    {
        SpostaTsunami();
        StartCoroutine(DurataAbilitaCoroutine());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Entita>() != null && collision.gameObject.GetComponent<Entita>() != Arena._instance.listaEntita[0])
        {
            Entita nemico = collision.gameObject.GetComponent<Entita>();

            nemico.ChangeElementValue(3, danniTipoAcqua);
            nemico.CheckIfABIsFull(3);
        }
    }

    IEnumerator DurataAbilitaCoroutine()
    {
        yield return new WaitForSeconds(DurataAbilita);
        Arena._instance.StartMoveAll();
        Destroy(this.gameObject);
    }

    void SpostaTsunami()
    {
        this.transform.position += direzioneSpostamento * Time.deltaTime * velocitāSpostamento;
    }
}
