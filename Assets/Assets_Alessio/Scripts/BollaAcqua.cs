using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BollaAcqua : Acqua1
{
    [SerializeField] float durataAbilita;

    [SerializeField] float danniAcqua;

    [SerializeField] float forzaLancio;

    [SerializeField] float timer;

    [SerializeField] float oldVita;



    Entita entitaDaDanneggiare;


    public Entita EntitaDaDanneggiare { get => entitaDaDanneggiare; set => entitaDaDanneggiare = value; }

    private void Start()
    {
        StartCoroutine(DanneggiaNemici());
    }

    private void Update()
    {
        Destroy(this.gameObject, durataAbilita);
    }

    IEnumerator DanneggiaNemici()
    {
        while (this)
        {
            EntitaDaDanneggiare.ChangeElementValue(3, danniAcqua);

            EntitaDaDanneggiare.CheckIfABIsFull(3);

            yield return new WaitForSeconds(timer);
        }
        yield return null;
    }

    public override void DanneggiaNemico()
    {
        entitaDaDanneggiare.gameObject.GetComponent<Rigidbody2D>().AddForce((entitaDaDanneggiare.gameObject.transform.position - Arena._instance.listaEntita[0].transform.position).normalized * forzaLancio);
    }
}
