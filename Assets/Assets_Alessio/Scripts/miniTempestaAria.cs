using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class miniTempestaAria : MonoBehaviour
{
    [SerializeField] float durataAbilita;

    [SerializeField] float danniAria;

    [SerializeField] float distanzaMassima;

    [SerializeField] float timer;

    [SerializeField] LayerMask layerNemici;

    private void Start()
    {
        StartCoroutine(DanneggiaNemici());
    }

    private void Update()
    {
        Destroy(this.gameObject, durataAbilita);
    }


    IEnumerator DanneggiaNemici()
    {
        while(this)
        {
            Collider2D[] nemici = Physics2D.OverlapCircleAll(transform.position, distanzaMassima, layerNemici);

            for(int i = 0; i< nemici.Length; i++)
            {
                nemici[i].gameObject.GetComponent<Entita>().ChangeElementValue(2, danniAria);
            }

            yield return new WaitForSeconds(timer);
        }

        yield return null;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Proiettile>() != null)
        {
            collision.gameObject.GetComponent<Proiettile>().GoBack();
        }
    }
}
