using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PallaDiFuoco : MonoBehaviour
{
    Entita targetNemico = null;

    [SerializeField] float danniTipoFuoco = 10f, danniAreaTipoFuoco = 5f, RaggioAreaDanni = 5f, speed = 0.2f;
    [SerializeField] LayerMask Nemici;

    [SerializeField] GameObject particles;
    GameObject newParticles = null;
    [SerializeField] GameObject explosion;

    Vector2 startPosition;

    float t = 0f;

    public Entita TargetNemico { get => targetNemico; set => targetNemico = value; }

    private void Start()
    {
        startPosition = transform.position;
        newParticles = Instantiate(particles, transform.position, Quaternion.identity);
    }

    private void Update()
    {
        newParticles.transform.position = transform.position;

        transform.position = Vector2.Lerp(startPosition, targetNemico.gameObject.transform.position, t);

        if (targetNemico != null)
        {
            if(t < 1)
            {
                t += Time.deltaTime * speed;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Entita>() != null && collision.gameObject.GetComponent<Entita>() != Arena._instance.listaEntita[0])
        {
            collision.gameObject.GetComponent<Entita>().ChangeElementValue(0, danniTipoFuoco);

            Collider2D[] nemici = Physics2D.OverlapCircleAll(transform.position, RaggioAreaDanni, Nemici);

            for (int i = 0; i < nemici.Length; i++)
            {
                //Debug.Log(nemici[i].name);
                nemici[i].gameObject.GetComponent<Entita>().ChangeElementValue(0, danniAreaTipoFuoco);
            }

            Instantiate(explosion, transform.position, Quaternion.identity);

            Destroy(this.gameObject);
        }
    }

}


