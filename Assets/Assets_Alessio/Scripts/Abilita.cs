using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abilita : MonoBehaviour
{
    virtual public void Attacca(Entita player, bool _isGrounded)
    {
       
    }

    virtual public void StopAbilita()
    {
        return;
    }

    virtual public void DanneggiaNemico()
    {
        return;
    }
}
