using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuoco1 : Abilita
{
    [SerializeField] GameObject palla;

    public override void Attacca(Entita player, bool _isGrounded)
    {
        Entita entitaVicina = null;
        float distanzaMinore = 1000;
        //Debug.Log("Fuoco1");

        for (int i = 1; i < Arena._instance.listaEntita.Count; i++)
        {
            if (Vector2.Distance(player.gameObject.transform.position, Arena._instance.listaEntita[i].gameObject.transform.position) < distanzaMinore)
            {
                entitaVicina = Arena._instance.listaEntita[i];
                distanzaMinore = Vector2.Distance(player.gameObject.transform.position, Arena._instance.listaEntita[i].gameObject.transform.position);
                //Debug.Log(entitaVicina + distanzaMinore.ToString());
            }
        }

        GameObject newpalla = Instantiate(palla, player.gameObject.transform.position, Quaternion.identity);
        //Debug.Log("Spawn");

        newpalla.GetComponent<PallaDiFuoco>().TargetNemico = entitaVicina;

    }
}
