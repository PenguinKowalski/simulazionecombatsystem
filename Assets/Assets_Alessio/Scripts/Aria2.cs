using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aria2 : Abilita
{
    [SerializeField] GameObject miniTempestaAria;

    public override void Attacca(Entita player, bool _isGrounded)
    {
        GameObject newMiniTempestaAria = Instantiate(miniTempestaAria, player.gameObject.transform);
        newMiniTempestaAria.transform.parent = player.gameObject.transform;
    }
}
