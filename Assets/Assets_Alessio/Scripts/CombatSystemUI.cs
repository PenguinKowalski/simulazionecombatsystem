using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatSystemUI : MonoBehaviour
{
    [SerializeField] Image pallinoVita;

    [SerializeField] Canvas canvas;

    [SerializeField] Image elementoCorrente;

    int cuoriDaTogliere;

    List<Image> vita = new List<Image>();

    [SerializeField] List<Sprite> elemento = new List<Sprite>();

    [SerializeField] Slider manaSlide;

    float maxVita;




    // Start is called before the first frame update
    void Start()
    {
        float n = 0;
        maxVita = Arena._instance.listaEntita[0].Vita;

        for (int i = 0; i < Arena._instance.listaEntita[0].Vita; i++)
        {
            if(i == 0)
            {
                Image newImage = Instantiate(pallinoVita, new Vector3(n + 1850, 980, 0f), Quaternion.identity);
                vita.Add(newImage);
                newImage.transform.parent = canvas.transform;

            }
            else
            {
                n -= 75f;
                Image newImage = Instantiate(pallinoVita, new Vector3(n + 1850, 980, 0f), Quaternion.identity);
                vita.Add(newImage);
                newImage.transform.parent = canvas.transform;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        RemoveVita();
        UpdateVita();
        if(Arena._instance.listaEntita[0] != null)
        {
            if (Arena._instance.listaEntita[0] is Player)
            {
                elementoCorrente.sprite = elemento[((Player)Arena._instance.listaEntita[0]).CurrentElement];
                manaSlide.value = ((Player)Arena._instance.listaEntita[0]).Mana;
            }
          
        }
      
    }

    void RemoveVita()
    {
        float diff = maxVita - Arena._instance.listaEntita[0].Vita;

        cuoriDaTogliere = Mathf.FloorToInt(diff);
    }

    void UpdateVita()
    {
        for(int i = 0; i <= cuoriDaTogliere; i++)
        {
            vita[vita.Count -1 - i].gameObject.SetActive(false);
        }
    }
}
