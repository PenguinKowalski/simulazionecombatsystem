using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Round", menuName = "ScriptableObjects/RoundScriptableObject", order = 1)]
public class Round : ScriptableObject
{
    [SerializeField] int[] numeroSpawnNemici;

    [SerializeField] int rangeRandom;

    public int[] NumeroSpawnNemici { get => numeroSpawnNemici; set => numeroSpawnNemici = value; }
    public int RangeRandom { get => rangeRandom; set => rangeRandom = value; }
}
