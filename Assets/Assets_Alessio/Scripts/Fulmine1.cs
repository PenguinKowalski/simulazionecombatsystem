using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fulmine1 : Abilita
{
    [SerializeField] float danniTipoFulmine;

    [SerializeField] GameObject effettoFulmine;


    public override void Attacca(Entita player, bool _isGrounded)
    {
        Entita entitaVicina = null;
        float distanzaMinore = 1000;

        for (int i = 1; i < Arena._instance.listaEntita.Count; i++)
        {
            if (Vector2.Distance(player.gameObject.transform.position, Arena._instance.listaEntita[i].gameObject.transform.position) < distanzaMinore)
            {
                entitaVicina = Arena._instance.listaEntita[i];
                distanzaMinore = Vector2.Distance(player.gameObject.transform.position, Arena._instance.listaEntita[i].gameObject.transform.position);
            }
        }

        GameObject newEffettoFulmine = Instantiate(effettoFulmine, entitaVicina.gameObject.transform.position, Quaternion.identity);
        entitaVicina.ChangeElementValue(1, danniTipoFulmine);

    }
}
