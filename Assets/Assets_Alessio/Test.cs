using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField] Abilita abilita;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            abilita.Attacca(Arena._instance.listaEntita[0], true);
        }
    }
}
