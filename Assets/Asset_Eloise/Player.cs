using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entita
{
    [Tooltip("Valore minimo di salto")]
    [SerializeField]
    float saltoMin;
    [Tooltip("Tempo massimo per aumentare l'altezza del salto")]
    [SerializeField]
    float saltoMax;
    [SerializeField] float salto,  mana, dashSpeed, jumpStrength,  attackTime;
    float manaMax = 75, mana1 = 25, mana2 = 50, timer, attackTimer, attackCooldown;
    [SerializeField] int saltoMassimo;
    int currentElement, indiceAbilita, saltoCorrente;
    List<Abilita> listaAbilita = new List<Abilita>();
    bool isAttacking, isJumping, failedCombo;
    Abilita abilitainuso;
    enum playerStates {idle, moving, jumping, dashing, hanging}
    playerStates state;

    public int CurrentElement { get => currentElement; set => currentElement = value; }
    public float Mana { get => mana; set => mana = value; }

    // Start is called before the first frame update
    public override void Start()
    {
        canMove = true;
        quantitaElementoAttuale = new float[4];
        resistenze = new float[5] { 1, 1, 1, 1, 1 };
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        isAttacking = false;
        InputRequests();
        ChangeElement();
        if (state == playerStates.idle)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        if (startAttack)
        {
            attackTimer += Time.deltaTime;
        }
        if(attackTimer > attackTime)
        {
            attackTimer = 0;
            startAttack = false;
            currentAttack = 0;
        }
        if (failedCombo)
        {
            attackCooldown -= Time.deltaTime;
        }
        if(attackCooldown <= 0)
        {
            failedCombo = false;
            attackCooldown = 0;
        }
        if(attackCooldown == 0)
        {
            attackCooldown = attackTime;
        }

        if (state== playerStates.idle || state == playerStates.moving)
        {
            if (!isGrounded)
                state = playerStates.jumping;
        }
        if (state == playerStates.jumping)
        {
            if (isGrounded)
            {
                saltoCorrente = 0;
                state = playerStates.idle;
            }
        }
        

        //Modifiche per Animator fatte da Ale22Gaming;
        if (state == playerStates.moving)
        {
            animator.SetBool("Move AD", true);
        }
        else
        {
            animator.SetBool("Move AD", false);
        }
        if (isGrounded == true)
        {
            animator.SetBool("Is grounded", true);
        }
        else if (isGrounded == false)
        {
            animator.SetBool("Is grounded", false);
        }
        if(Vita < 0)
        {
            animator.SetBool("Death", true);
        }
        if(isGrounded == false && rb.velocity.y > 0)
        {
            animator.SetBool("Jump", true);
        }
        if (isGrounded == false && rb.velocity.y < 0)
        {
            animator.SetBool("Falling", true);
        }


    }
    private void FixedUpdate()
    {
        if (isJumping)
        {
            timer += Time.fixedDeltaTime;
            if (timer <= saltoMax)
            {
                rb.AddForce(new Vector2(0, salto), ForceMode2D.Force);
            }
        }
        if ((state == playerStates.moving || state == playerStates.jumping) && canMove)
        {
            rb.velocity = new Vector2(movementVelocity.x, rb.velocity.y);
        }
    }

    void InputRequests()
    {
        if (!isAttacking && state != playerStates.hanging)
        {
            float velocity = velocita * Input.GetAxis("Horizontal");
            Move(velocity);
            if(velocity != 0 && state == playerStates.idle)
            {
                state = playerStates.moving;
            }
            if(velocity == 0 && state == playerStates.moving)
            {
                state = playerStates.idle;
            }
            if (Input.GetButtonDown("Vertical") && state != playerStates.jumping)
            {
                Dash();
            }
        }
        if (Input.GetButtonDown("Jump"))
        {
            float directionX = 0;
            if (state == playerStates.hanging)
            {
                rb.isKinematic = false;
                directionX = -transform.right.x * jumpStrength;
                state = playerStates.jumping;
            }
            Jump(directionX);
        }
        if (Input.GetButtonUp("Jump"))
        {
            isJumping = false;
            timer = 0;
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (currentElement > 0)
            {
                UsaAbilita(indiceAbilita, isGrounded);
            }
        }

        if (state == playerStates.hanging)
        {
            if (isGrounded)
            {
                saltoCorrente = 0;

            }
        }
        if (Input.GetButtonDown("Fire1"))
        {
            Attack();
        }

    }

    void Dash()
    {
        Debug.DrawRay(transform.position, transform.right * dashSpeed, Color.magenta, 20f);
        if (!Physics2D.Raycast(transform.position, transform.right, dashSpeed, hitMe))
        {
            rb.position = transform.position + (transform.right * dashSpeed * 0.9f);
        }
    }

    void Jump(float directionX)
    {
            if (saltoCorrente < saltoMassimo)
            {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(new Vector2(directionX, saltoMin), ForceMode2D.Impulse);
            saltoCorrente++;
            isJumping = true;
            timer = 0;
            }
    }
    void ChangeElement()
    {
        if (Input.GetButtonDown("Fuoco") && !(currentElement == 1))
        {
            currentElement = 1;
        }
        else if (Input.GetButtonDown("Fuoco") && (currentElement == 1))
        {
            currentElement = 0;
        }
        if (Input.GetButtonDown("Fulmine") && !(currentElement == 2))
        {
            currentElement = 2;
        }
        else if (Input.GetButtonDown("Fulmine") && (currentElement == 2))
        {
            currentElement = 0;
        }
        if (Input.GetButtonDown("Aria") && !(currentElement == 3))
        {
            currentElement = 3;
        }
        else if (Input.GetButtonDown("Aria") && (currentElement == 3))
        {
            currentElement = 0;
        }
        if (Input.GetButtonDown("Acqua") && !(currentElement == 4))
        {
            currentElement = 4;
        }
        else if (Input.GetButtonDown("Acqua") && (currentElement == 4))
        {
            currentElement = 0;
        }

    }
    void UsaAbilita(int _indiceabilita, bool _isGrounded)
    {
        if (currentElement == 1)
        {
            if (mana >= mana1 && mana < mana2 || _isGrounded == false)
            {
                _indiceabilita = 0;
            }
            if (_isGrounded == true)
            {
                if (mana >= mana2 && mana < manaMax)
                {
                    _indiceabilita = 1;
                }
                if (mana == manaMax)
                {
                    _indiceabilita = 2;
                }
            }
        }

        if (currentElement == 2)
        {
            if (mana >= mana1 && mana < mana2 || _isGrounded == false)
            {
                _indiceabilita = 3;
            }
            if (mana >= mana2 && mana < manaMax)
            {
                _indiceabilita = 4;
            }
            if (mana == manaMax)
            {
                _indiceabilita = 5;
            }
        }
        if (_isGrounded == true)
        {
            if (currentElement == 3)
            {
                if (mana >= mana1 && mana < mana2)
                {
                    _indiceabilita = 6;
                }
                if (mana >= mana2 && mana < manaMax)
                {
                    _indiceabilita = 7;
                }
                if (mana == manaMax)
                {
                    _indiceabilita = 8;
                }
            }
            if (currentElement == 4)
            {
                if (mana >= mana1 && mana < mana2)
                {
                    _indiceabilita = 9;
                }
                if (mana >= mana2 && mana < manaMax)
                {
                    _indiceabilita = 10;
                }
                if (mana == manaMax)
                {
                    _indiceabilita = 11;
                }
            }
        }
        if(mana >= mana1)
        {
            listaAbilita[_indiceabilita].Attacca(Arena._instance.GetPlayer(), _isGrounded);
            mana = 0;
        }
    }

    public override float ChangeHealth(float valoreVita)
    {
        base.ChangeHealth(valoreVita);
        if (abilitainuso)
        {
            abilitainuso.StopAbilita();
        }
        return vita;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("hangingWall"))
        {
            if(state==playerStates.jumping &&timer>0.05f)
            {
                state = playerStates.hanging;
                saltoCorrente = 0;
                rb.isKinematic = true;
                rb.velocity = Vector2.zero;
            }
        }
    }

    public override void Attack()
    {
        startAttack = true;
        if (currentAttack < 3)
        {
            currentAttack++;
        }
        //Modifiche per Animator fatte da Ale22Gaming;

        if (((currentAttack == 1 && attackTimer < 0.5f && attackTimer >= 0f) || (currentAttack == 2 && attackTimer < 1f && attackTimer > 0.5f) || (currentAttack == 3 && attackTimer < 1.5f && attackTimer > 1f))&& !failedCombo)
        {
            isAttacking = true;
            Debug.DrawRay(transform.position, transform.right * 0.7f, Color.magenta, 20f);

            if (raycastHit = Physics2D.Raycast(transform.position, transform.right, 0.7f, attackMask))
            {
                if (raycastHit != false)
                {
                    Debug.Log("Trovato collider");

                    Entita entitaHit = raycastHit.collider.GetComponent<Entita>();
                    if (entitaHit == null)
                    {
                        Debug.Log("Non � Entita");
                        Abilita abilitaHit = raycastHit.collider.GetComponent<Abilita>();
                        if (abilitaHit != null)
                        {
                            abilitaHit.DanneggiaNemico();
                        }
                    }
                    else
                    {
                        Debug.Log("E' Entita");

                        entitaHit.ChangeHealth(Danno);

                        mana += 25;
                    }
                }
            }

            Debug.Log("atacco");

            if (currentAttack == 1)
            {
                animator.Play("Attacco 1");
            }
            else if (currentAttack == 2)
            {
                animator.Play("Attacco 2");
            }
            else if (currentAttack == 3)
            {
                animator.Play("Attacco 3");
            }
        }
        else
        {
            startAttack = false;
            attackTimer = 0;
            currentAttack = 0;
            failedCombo = true;
            Debug.Log("sbagliato tempismo");
        }

    }
}

