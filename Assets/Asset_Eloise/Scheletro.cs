using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scheletro : Entita
{
    [SerializeField] float velocitaNemico;
    [SerializeField] Rigidbody2D playerSprite;
    [SerializeField] Vector2 dir;
    [SerializeField] float cooldown = 3;

    // Start is called before the first frame update


    void FixedUpdate()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.fixedDeltaTime;
        }
        else
        {
            cooldown = 0;
        }

        if (Vector2.Distance(transform.position, Arena._instance.listaEntita[0].transform.position) < 0.75f && cooldown <= 0)
        {
            Attack();
        }
    }
    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        dir = (transform.position - Arena._instance.listaEntita[0].transform.position).normalized;
        if (Vector2.Distance(transform.position, Arena._instance.listaEntita[0].transform.position) < 30f && canMove)
        {
            if (Vector2.Distance(transform.position, Arena._instance.listaEntita[0].transform.position) > 0.5f)
            {
                if (dir.x > 0.5f && isGroundedLeft)
                {
                    velocitaNemico = -1 * velocita;
                }
                if (dir.x < -0.5f && isGroundedRight)
                {
                    velocitaNemico = 1 * velocita;
                }
                if (dir.x > 0.5f && !isGroundedLeft)
                {
                    velocitaNemico = 0;
                }
                if (dir.x < -0.5f && !isGroundedRight)
                {
                    velocitaNemico = 0;
                }
                if (dir.x > -0.5f && dir.x < 0.5f)
                {
                    velocitaNemico = 0;
                }
            }
            else
            {
                velocitaNemico = 0;
            }
        }
        else
        {
            velocitaNemico = 0;
        }
        Move(velocitaNemico);
        rb.velocity = new Vector2(movementVelocity.x, rb.velocity.y);
    }
    public override void Attack()
    {
        base.Attack();
        cooldown = 3;
    }
}
