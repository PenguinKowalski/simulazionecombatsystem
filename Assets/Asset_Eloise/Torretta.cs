using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torretta : Entita
{
    [SerializeField]GameObject proiettile;
    [SerializeField] Rigidbody2D rb_projectile;
    [SerializeField] float cooldown, stateDuration;
    float stateTimer;
    Vector2 dir;
    bool canAttack;
    [SerializeField] SpriteRenderer enemySprite;

    enum enemyStates { normal, onFire, paralyzed, pushed, slowed }
    enemyStates state;

    public override void Start()
    {
        base.Start();
        state = enemyStates.normal;
    }
    // Update is called once per frame
    public override void Update()
    {
        canAttack = true;
        dir = (transform.position - Arena._instance.listaEntita[0].transform.position).normalized;
        if (dir.x > 0f)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        if (dir.x < 0f)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if(vita < 0)
        {
            animator.Play("Death");
        }
        StateChanges();

        if (stateTimer > 0 && stateTimer <= stateDuration)
        {

            if (state == enemyStates.onFire)
            {
                IsOnFire();
                enemySprite.color = Color.red;
            }
            if (state == enemyStates.paralyzed)
            {
                canMove = false;
                canAttack = false;
                enemySprite.color = Color.yellow;
            }
            if (state == enemyStates.pushed)
            {
                rb.AddForce(-dir);
                enemySprite.color = Color.gray;
            }
            if (state == enemyStates.slowed)
            {
                enemySprite.color = Color.blue;
            }
        }
        else
        {
            state = enemyStates.normal;
        }
    }
    void FixedUpdate()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.fixedDeltaTime;
        }
        else
        {
            cooldown = 0;
        }

        if (Vector2.Distance(transform.position, Arena._instance.listaEntita[0].transform.position) < 30f && cooldown <= 0)
        {
            StartCoroutine(TorrettaAttack());
        }
        if (state != enemyStates.normal)
        {
            stateTimer += Time.fixedDeltaTime;
        }
        else
        {
            stateTimer = 0;
        }
    }

    IEnumerator TorrettaAttack()
    {
        cooldown = 2f;
        animator.Play("Attack");
        if (state == enemyStates.slowed)
        {
            yield return new WaitForSeconds(1.7f);
        }
        if (state == enemyStates.normal)
        {
            yield return new WaitForSeconds(3.4f);
        }

        Spara();
        yield return null;
    }

    void Spara()
    {
        Debug.Log("Sparo");
        GameObject go_projectile = GameObject.Instantiate(proiettile, transform.position, transform.rotation);
        go_projectile.transform.parent = this.transform;
        rb_projectile = go_projectile.gameObject.GetComponent<Rigidbody2D>();
        rb_projectile.AddForce(( Arena._instance.listaEntita[0].transform.position - transform.position).normalized * velocita, ForceMode2D.Impulse);
    }

    void StateChanges()
    {
        if (quantitaElementoAttuale[0] >= vita)
        {
            state = enemyStates.onFire;
        }
        if (quantitaElementoAttuale[1] >= vita)
        {
            state = enemyStates.paralyzed;
        }
        if (quantitaElementoAttuale[2] >= vita)
        {
            state = enemyStates.pushed;
        }
        if (quantitaElementoAttuale[3] >= vita)
        {
            state = enemyStates.slowed;
        }
    }
    IEnumerator IsOnFire()
    {
        vita -= 1f;
        yield return new WaitForSeconds(1f);
        yield return null;
    }
}
