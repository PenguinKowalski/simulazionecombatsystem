using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : Entita
{
    [SerializeField] float cooldown, inputVelocitaNemico, stateDuration, delayAttack;
    Vector2 dir;
    float  stateTimer, velocitaNemico;
    bool canAttack, canDelay;
    [SerializeField] SpriteRenderer enemySprite;
    enum enemyStates { normal, onFire, paralyzed, pushed, slowed }
    enemyStates state;
    public override void Start()
    {
        base.Start();
        state = enemyStates.normal;
    }

    void FixedUpdate()
    {
        if(cooldown > 0)
        {
            cooldown -= Time.fixedDeltaTime;
        }
        else
        {
            cooldown = 0;
        }
        if (canDelay)
        {
            delayAttack += Time.fixedDeltaTime;
        }
        else
        {
            delayAttack = 0;
        }
       
        if (Vector2.Distance(transform.position, Arena._instance.listaEntita[0].transform.position) < 0.75f && cooldown <= 0 && canAttack)
        {
            animator.Play("Attack");
            canDelay = true;
            cooldown = 3f;
        }
        if (delayAttack >= 0.85f)
        {
            Attack();
            canDelay = false;
        }

        if (rb.velocity.x > 0.1f || rb.velocity.x < -0.1f)
        {
            animator.SetBool("Move", true);
        }
        else
        {
            animator.SetBool("Move", false);
        }
        if (state != enemyStates.normal)
        {
            stateTimer += Time.fixedDeltaTime;
        }
        else
        {
            stateTimer = 0;
        }

    }

    // Update is called once per frame
    public override void Update()
    {
        canMove = true;
        velocitaNemico = inputVelocitaNemico;
        canAttack = true;
        base.Update();

        dir = (transform.position - Arena._instance.listaEntita[0].transform.position).normalized;

        if(Vector2.Distance(transform.position, Arena._instance.listaEntita[0].transform.position) < 30f && canMove)
        {
            if (Vector2.Distance(transform.position, Arena._instance.listaEntita[0].transform.position) > 0.5f)
            {
                if (dir.x > 0.5f && isGroundedLeft)
                {
                    velocitaNemico = -1 * velocita;
                }
                if (dir.x < -0.5f && isGroundedRight)
                {
                    velocitaNemico = 1 * velocita;
                }
                if (dir.x > 0.5f && !isGroundedLeft)
                {
                    velocitaNemico = 0;
                }
                if (dir.x < -0.5f && !isGroundedRight)
                {
                    velocitaNemico = 0;
                }
                if (dir.x > -0.5f && dir.x < 0.5f)
                {
                    velocitaNemico = 0;
                }
            }
            else
            {
                velocitaNemico = 0;            
            }
        }
        else
        {
            velocitaNemico = 0;
        }

        Move(velocitaNemico);

        rb.velocity = new Vector2(movementVelocity.x, rb.velocity.y);

        if(vita < 0)
        {
            animator.Play("Death");
        }
        StateChanges();

        if(stateTimer > 0 && stateTimer <= stateDuration)
        {
            if(state == enemyStates.onFire)
            {
                IsOnFire();
                enemySprite.color = Color.red;
            }
            if (state == enemyStates.paralyzed)
            {
                canMove = false;
                canAttack = false;
                enemySprite.color = Color.yellow;
            }
            if (state == enemyStates.pushed)
            {
                rb.AddForce(-dir);
                enemySprite.color = Color.gray;
            }
            if (state == enemyStates.slowed)
            {
                velocitaNemico = velocitaNemico / 2;
                enemySprite.color = Color.blue;
            }
        }
        else
        {
            state = enemyStates.normal;
        }

    }
    public override void Attack()
    {
        //animator.Play("Attack");
        base.Attack();
        cooldown = 3;
    }

   /* IEnumerator AttackOnAnimEnd()
    {
        Attack();
        yield return new WaitForSeconds(1f);
        yield return null;
    }*/
    void StateChanges()
    {
        if (quantitaElementoAttuale[0] >= vita)
        {
            state = enemyStates.onFire;
        }
        if (quantitaElementoAttuale[1] >= vita)
        {
            state = enemyStates.paralyzed;
        }
        if (quantitaElementoAttuale[2] >= vita)
        {
            state = enemyStates.pushed;
        }
        if (quantitaElementoAttuale[3] >= vita)
        {
            state = enemyStates.slowed;
        }
    }
    IEnumerator IsOnFire()
    {
        vita -= 1f;
        yield return new WaitForSeconds(1f);
        yield return null;
    }
}
