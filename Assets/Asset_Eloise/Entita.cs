using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entita : MonoBehaviour
{
    [SerializeField] protected float velocita, valoreMassimoVita;
    protected int currentAttack;
    protected bool canMove,  startAttack;
    public bool isGroundedLeft, isGroundedRight, isGrounded;
    [SerializeField] protected float[] quantitaElementoAttuale;
    [SerializeField] protected float[] resistenze;
    [SerializeField] protected Animator animator;
    [SerializeField] protected Rigidbody2D rb;
    protected Vector2 rbVelocity,  movementVelocity;
    [SerializeField] protected int elemento;
    [SerializeField] protected LayerMask hitMe;
    protected RaycastHit2D raycastHit;
    protected ContactFilter2D contactFilter;
    [SerializeField] protected LayerMask attackMask;
    [SerializeField]
    protected float vita;
    [SerializeField]
    private float danno;

    public float Vita { get => vita; set => vita = value; }
    protected float Danno { get => danno; set => danno = value; }

    // Start is called before the first frame update
    public virtual void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        quantitaElementoAttuale = new float[4];
        resistenze = new float[5] {1,1,1,1,1};
    }

    // Update is called once per frame
    public virtual void Update()
    {
        IsGroundedLeft();
        IsGroundedRight();
        movementVelocity.x = 0;
        if (!isGroundedRight && !isGroundedLeft)
        {
            isGrounded = false;
        }
        else
        {
            isGrounded = true;
        }
        Death(vita);
    }
    public virtual void Move(float _velocita)
    {
        movementVelocity.x = _velocita;
        if(_velocita != 0)
        {
            if (_velocita > 0)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }  
    }


    public virtual void Attack()
    {
   
        Debug.DrawRay(transform.position, transform.right * 0.7f, Color.magenta, 20f);
        if (raycastHit = Physics2D.Raycast(transform.position, transform.right, 0.7f, attackMask))
        {
            if (raycastHit != false)
            {
                Debug.Log("Trovato collider");

                Entita entitaHit = raycastHit.collider.GetComponent<Entita>();
                if (entitaHit == null)
                {
                    Debug.Log("Non � Entita");
                    Abilita abilitaHit = raycastHit.collider.GetComponent<Abilita>();
                    if (abilitaHit != null)
                    {
                        abilitaHit.DanneggiaNemico();
                    }
                }
                else
                {
                    Debug.Log("E' Entita");
                    Debug.Log(this.name + " ha attaccato " + entitaHit.name);
                    entitaHit.ChangeHealth(Danno);
                }
            }
        }
    }

    public virtual float ChangeHealth(float valoreVita)
    {  
        vita -= resistenze[0] * valoreVita;
        return vita;
    }

    public void CheckIfABIsFull( int _elemento)
    {
        if(quantitaElementoAttuale[_elemento] == valoreMassimoVita)
        {
            Arena._instance.RemoveEntity(this);
        }
    }

    public virtual void Death(float _vita)
    {
        if(_vita <= 0 && !Arena._instance.listaEntita[0])
        {
            Arena._instance.RemoveEntity(this);
            Destroy(this.gameObject, 3f);
        }
    }

    public void ChangeElementValue(int _elemento, float qntElemento)
    {
        quantitaElementoAttuale[_elemento] -= qntElemento;
    }

    public void StartMove()
    {
        rb.velocity = rbVelocity;
        rb.isKinematic = false;
        canMove = true;
    }

    public void StopMove()
    {
        rbVelocity = rb.velocity;
        rb.velocity = Vector2.zero;
        rb.isKinematic = true;
        canMove = false;
    }

    void KnockBack(float forza, Vector2 direzione)
    {
        direzione.x += direzione.x * forza;
    }

    public void ChangeResistenza(float newResistenza, int indice_resistenza)
    {
        resistenze[indice_resistenza] = newResistenza;
    }

    bool IsGroundedRight()
    {
        Debug.DrawRay(new Vector2(transform.position.x + 0.2f, transform.position.y - 0.2f), Vector2.down * 0.25f, Color.white);
        if (Physics2D.Raycast(new Vector2(transform.position.x + 0.2f, transform.position.y - 0.2f), Vector2.down, 0.25f, hitMe))
        {
            isGroundedRight = true;                     
        }
        else
        {
            isGroundedRight = false;        
        }
        return isGroundedRight;
    }

    bool IsGroundedLeft()
    {
        Debug.DrawRay(new Vector2(transform.position.x - 0.2f, transform.position.y - 0.2f), Vector2.down * 0.25f, Color.green);
        if (Physics2D.Raycast(new Vector2(transform.position.x - 0.2f, transform.position.y - 0.2f), Vector2.down, 0.25f,hitMe))
        {
            isGroundedLeft = true;    
        }
        else
        {
            isGroundedLeft = false;
        }
        return isGroundedLeft;
    }
}
